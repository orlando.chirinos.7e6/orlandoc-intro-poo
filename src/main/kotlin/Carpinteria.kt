import java.util.*
val scan = Scanner(System.`in`)
//======================================================================================================================
fun main(){
    carpinteria()
}
class Taulell(
    var precio: Double,
    var largo: Double,
    var ancho: Double
){
    fun calcularPrecio():Double{
        val precioTotal = (largo*ancho)*precio
        return precioTotal
    }
}
class Llisto(
    var precio: Double,
    var largo: Double
){
    fun calcularPrecio():Double{
        val precioTotal = largo*precio
        return precioTotal
    }
}

fun carpinteria(){

    print("Introduzca cantidad de productos que desea comprar: ")
    val amountProducts = scan.nextInt()

    var limit = 0
    var totalPriceT = 0.0
    var totalPriceL = 0.0

    val totalPrice: Double

    do {

        print("Introduzca tipo de producto [T] / [L]: ")
        var productType = scan.next().toUpperCase()

        while (productType != "T" && productType != "L"){
            print("¡PRODUCTO INEXISTENTE! SELECCIONE UNA OPCIÓN VÁLIDA [T] / [L]: ")
            productType = scan.next().toUpperCase()
        }
        if (productType == "T") {

            print("Introduzca largo del producto: ")
            val long = scan.nextDouble()

            print("Introduzca ancho del producto: ")
            val width = scan.nextDouble()

            print("Introduzca precio base del producto: ")
            val price = scan.nextDouble()

            totalPriceT += Taulell(long,width,price).calcularPrecio()

            println("\nEl precio total del producto es: [$totalPriceT eur.]")
        }

        else if (productType == "L") {
            print("Introduzca largo del producto: ")
            val long = scan.nextDouble()

            print("Introduzca precio base del producto: ")
            val price = scan.nextDouble()

            totalPriceL += Llisto(long,price).calcularPrecio()

            println("\nEl precio total del producto es: [$totalPriceL eur.]")
        }

        limit++

    }while (limit != amountProducts)

    totalPrice = totalPriceT + totalPriceL

    println("=========================================================================================================\n" +
            "El precio total de todos los productos suma una cantidad de $totalPrice eur.")
}
//======================================================================================================================