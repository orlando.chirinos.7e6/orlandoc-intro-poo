//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

class Bombeta(
    var nom:String,
    val colors:Array<String> = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN),
    var color:Int =0,
    var offColor: Int =1,
    var intensitat:Int =0,
    var intensitatPujada:Boolean =true
) {

    //Funció que encèn la làmpada si aquesta està apagada, agafant com a color inicial el que es va guardar a l'apagar,  posa la intensitat a 1 i indica que la intensitat anirà de pujada
    fun turnOn() {
        if (color == 0) {
            color = offColor
            intensitat = 1
            intensitatPujada = true
        }
    }

    //Funció que apaga la làmpada si està encesa, guarda el color de la llum per a la propera vegada que s'encengui, posa el color a 0 (negre) per a indicar que està apagada, la intensitat la posa a 0, ja que la bombeta està apagada
    fun turnOff() {
        if (color > 0) {
            offColor = color
            color = 0
            intensitat = 0
        }
    }

    //Funció que modifica el color del llum de la bombeta si la làmpada està encesa (és a dir, el color no és el negre). Si el llum actual és el darrer (cyan), torna al primer (white)
    fun changeColor() {
        if (color != 0) {
            if (color <= colors.size - 2) color++
            else if (color == colors.size - 1) color = 1
        }
    }

    //Funció que modifica la intensitat de la bombeta si és que la làmpada està encesa. Si la intensitat està configurada de pujada (intensitatPujada=true), anirà pujant fins al 5, i si arriba al 5, indicarà que va de baixada (intensitatPujada=false) i anirà de baixada fins a l'1
    fun intensity() {
        if (color != 0) {
            if (intensitatPujada && intensitat < 5) intensitat++
            else if (intensitatPujada && intensitat == 5) {
                intensitatPujada = false
                intensitat--
            } else if (!intensitatPujada && intensitat > 1) intensitat--
            else if (!intensitatPujada && intensitat == 1) {
                intensitatPujada = true
                intensitat++
            }
        }
    }

    fun mostrarLlum() {
        println("#$nom# - Color: ${colors[color]}    $ANSI_RESET - intensitat $intensitat")
    }
}

fun chooselight():String{

    print("Seleccione luz:\n" +
            "[C] = CUINA\n" +
            "[M] = MENJADOR\n" +
            "[B] = BANY\n" +
            " > ")

    var choice = scan.next().toLowerCase()

    while (choice != "c" && choice != "m" && choice != "b"){
        println("¡Seleccione una opción válida!")

        print("Seleccione luz:\n" +
                "[C] = CUINA\n" +
                "[M] = MENJADOR\n" +
                "[B] = BANY\n" +
                " > ")

        choice = scan.next().toLowerCase()
    }
    return choice
}
fun main(){

    var choice = chooselight()

    val cuina = Bombeta("cuina")
    val menjador = Bombeta("menjador")
    val bany = Bombeta("bany")

    var place = Bombeta("")

    when (choice){
        "c" -> place = cuina
        "m" -> place = menjador
        "b" -> place = bany
    }

    do {
        print("Seleccione una orden: \n" +
                "TURN ON, TURN OFF, CHANGE COLOR, INTENSITY, SWITCH LIGHT: ")

        val instruction= readln().toUpperCase()
        when(instruction) {
            "TURN ON" -> place.turnOn()
            "TURN OFF" -> place.turnOff()
            "CHANGE COLOR" -> place.changeColor()
            "INTENSITY" -> place.intensity()
            "SWITCH LIGHT" -> {
                choice = chooselight()
                when (choice){
                    "c" -> place = cuina
                    "m" -> place = menjador
                    "b" -> place = bany
                }
            }
        }
        place.mostrarLlum()
    } while(instruction!="END")
}
