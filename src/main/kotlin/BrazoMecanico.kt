//5- MechanicalArmApp
class MechanicalArm() {

    var on: Boolean = false
    var angle: Int = 0
    var altitude: Int = 0

    fun toggle() {

        if (!on) {
            on = true
        } else {
            on = false
            angle = 0
            altitude = 0
        }
        status()

    }

    fun updateAngle(angleToSum: Int) {

        if (!on) {
            println("NO ES POSIBLE MODIFICAR NINGÚN PARÁMETRO CON EL BRAZO APAGADO")
        } else {
            angle += angleToSum

            if (angle > 360) {
                angle = 360
            } else if (angle < 0) {
                angle = 0
            }
            status()
        }
    }

    fun updateAltitude(altitudeToSum: Int) {
        if (!on) {
            println("NO ES POSIBLE MODIFICAR NINGÚN PARÁMETRO CON EL BRAZO APAGADO")
        } else {
            altitude += altitudeToSum

            if (altitude > 30) {
                altitude = 30
            } else if (altitude < 0) {
                altitude = 0
            }
            status()
        }
    }

    fun status() {

        println("MechanicalArm{Angulo: $angle, Altitud: $altitude, TurnedOn: $on}")

    }

}

fun main(args: Array<String>) {

    var brazo = MechanicalArm()

    brazo.toggle()
    brazo.updateAltitude(3)
    brazo.updateAngle(180)
    brazo.updateAltitude(-3)
    brazo.updateAngle(-180)
    brazo.updateAltitude(3)
    brazo.toggle()

}