//======================================================================================================================
fun main(){
    pasteleria()
}
class Pasta(
    val nombre: String,
    val precio: Double,
    val peso: Double,
    val calorias: Int
) {
    fun mostrar(){
        println("Nom: $nombre")
        println("Precio: $precio€")
        println("Peso: $peso g")
        println("Calorias: $calorias kcal")
        println("==========================")
    }
}

class Bebida(
    val nombre: String,
    var precio: Double,
    val azucar: Boolean
){
    fun mostrar(){
        println("Nom: $nombre")
        println("Precio: ${aumentado()} eur.")
        println("Azucar: $azucar ")
        println("==========================")
    }

    fun aumentado():Double{

        var aumento = 0.00
        if (azucar == true){
            aumento = precio * 1.10 }
        else{aumento = precio}
        return aumento
    }
}

fun pasteleria() {

    val donut = Pasta("Donut", 1.0, 0.2, 2200)
    val ensaimada = Pasta("Ensalada", 0.85, 0.6, 2000)
    val croisant = Pasta("Quaso", 0.9, 0.5, 2100)

    donut.mostrar()
    ensaimada.mostrar()
    croisant.mostrar()

    val agua = Bebida("Agua", 1.00, false)
    val cafe = Bebida("Café cortado (como mis ganas de vivir)", 1.35, false)
    val te = Bebida("Té", 1.50, false)
    val cola = Bebida("Cola", 1.65, true)

    agua.mostrar()
    cafe.mostrar()
    te.mostrar()
    cola.mostrar()
}
//======================================================================================================================